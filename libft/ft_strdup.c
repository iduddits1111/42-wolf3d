/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 11:57:56 by mborde            #+#    #+#             */
/*   Updated: 2014/11/28 22:09:16 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strdup(const char *s1)
{
	int		i;
	char	*dst;

	i = 0;
	if ((dst = (char*)malloc(sizeof(char) * ft_strlen(s1) + 1)))
		dst[ft_strlen(s1)] = '\0';
	else
		return (0);
	while (s1[i])
	{
		dst[i] = s1[i];
		i++;
	}
	return (dst);
}
