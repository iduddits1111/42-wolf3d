/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 18:54:52 by mborde            #+#    #+#             */
/*   Updated: 2014/11/10 22:20:29 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstnew(const void *content, size_t content_size)
{
	t_list	*newl;
	void	*cont;

	cont = (void *)ft_memalloc(content_size);
	if (content == NULL)
	{
		cont = NULL;
		content_size = 0;
	}
	else
		cont = ft_memcpy(cont, content, content_size);
	if ((newl = (t_list *)ft_memalloc(sizeof(t_list))))
	{
		newl->content_size = content_size;
		newl->next = NULL;
		newl->content = cont;
	}
	return (newl);
}
