/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 14:47:42 by mborde            #+#    #+#             */
/*   Updated: 2015/06/09 22:43:14 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strnew(size_t size)
{
	char	*s;

	if ((s = malloc(sizeof(size_t) * size + 1)) && size < SIZE_MAX)
	{
		ft_bzero(s, size);
		return (s);
	}
	return (NULL);
}
