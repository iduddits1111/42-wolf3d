/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 10:54:26 by mborde            #+#    #+#             */
/*   Updated: 2015/01/02 17:52:49 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	what_sign(const char *str)
{
	int i;
	int sign;

	i = 0;
	sign = 1;
	while (str[i] && !ft_isdigit(str[i]))
	{
		if (str[i] == '-')
			sign = -1;
		else if (str[i] == '+')
			sign = 1;
		i++;
	}
	return (sign);
}

static int	ft_isschar(char c)
{
	if (c == ' ' || c == '\t' || c == '\n'
			|| c == '\v' || c == '\r' || c == '\f')
		return (1);
	else
		return (0);
}

int			ft_atoi(const char *str)
{
	char	*s;
	int		n;
	int		i;
	int		sign;

	n = 0;
	i = 0;
	s = ft_strdup(str);
	sign = what_sign(s);
	while (ft_isschar(s[i]))
		i++;
	if (s[i] == '+' || s[i] == '-')
		i++;
	while (ft_isdigit(s[i]))
	{
		n *= 10;
		n += (s[i] - '0');
		i++;
	}
	return (n * sign);
}
