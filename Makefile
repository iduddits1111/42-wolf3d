# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mborde <mborde@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/10 10:52:33 by mborde            #+#    #+#              #
#    Updatd: 2015/02/10 12:05:05 by mborde           ###   ########.fr         #
#                                                                              #
# **************************************************************************** #

NAME = wolf3d

SRCS = main.c		\
	   get_map.c	\
	   cast.c		\
	   draw.c		\
	   events.c		\
	   move.c		\
	   sound.c		\
	   text.c		\
	   texture.c	\
	   objectives.c	\
	   sprite.c		\
	   help.c

INCS = -I libft/								\
	   -I incs/									\
	   -I install/include/SDL2/					\
	   -I install/freetype/include/freetype2/

OBJS = $(patsubst %.c, obj/%.o, $(SRCS))

FLAG = -O3 -Wall -Wextra -Werror

LIBFT = libft/libft.a

SDL = `./install/bin/sdl2-config --cflags --libs`

FREETYPE = `./install/freetype/bin/freetype-config --cflags --libs`

MIXER = -lSDL2_mixer

TTF = -lSDL2_ttf

LSDL = install/lib/libSDL2.a				\
	   install/lib/libSDL2_mixer.a			\
	   install/lib/libSDL2_ttf.a			\
	   install/freetype/lib/libfreetype.a

all: $(NAME)

$(NAME): $(LSDL) $(LIBFT) obj $(OBJS)
	@echo "Compiling wolf3d..."
	@gcc $(FLAG) -o $@ $(OBJS) -L libft/ -lft $(SDL) $(MIXER) $(TTF) $(FREETYPE)
	@echo "		\033[32mOK\033[0m"

obj/%.o: srcs/%.c
	@gcc $(FLAG) $(INCS) -o $@ -c $<
	@echo "[\033[32m√\033[m]" $@

obj:
	@mkdir -p obj

$(LIBFT):
	@echo "Building libft..."
	@make -C libft/
	@echo "		\033[32mOK\033[0m"

$(LSDL):
	@mkdir -p install
	@mkdir -p install/freetype
	@cd sdl-sources; ./configure --prefix=`cd ../install && pwd`
	@make -C sdl-sources/
	@make -C sdl-sources/ install
	@cd freetype-sources; ./configure --prefix=`cd ../install/freetype	\
		&& pwd` --with-sdl-prefix=`cd ../install/ && pwd`
	@make -C freetype-sources/
	@make -C freetype-sources/ install
	@cd sdl-mixer; ./configure --prefix=`cd ../install && pwd`
	@make -C sdl-mixer/
	@make -C sdl-mixer/ install
	@cd sdl-ttf; ./configure --prefix=`cd ../install && pwd`			\
		--with-freetype-prefix=`cd ../install/freetype && pwd`
	@make -C sdl-ttf/
	@make -C sdl-ttf/ install
	@echo "		\033[32mOK\033[0m"

libfc:
	@make -C libft/ fclean

clean:
	@echo "Removing object files."
	@/bin/rm -rf obj/

fclean: clean
	@echo "Removing binary."
	@/bin/rm -f $(NAME)

sdlfc:
	@make -C sdl-sources/ clean
	@make -C sdl-mixer/ clean
	@make -C sdl-ttf/ clean
	@make -C freetype-sources/ clean
	@/bin/rm -rf install

purge: fclean libfc sdlfc

re: fclean all
