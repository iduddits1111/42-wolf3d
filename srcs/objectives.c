/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objectives.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/09 15:20:30 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 20:24:28 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		key_pos(t_wolf *w)
{
	srand(time(NULL));
	w->s.kx = rand() % 23;
	w->s.ky = rand() % 23;
	while (w->map[w->s.kx][w->s.ky] != 0)
	{
		w->s.kx = rand() % 23;
		w->s.ky = rand() % 23;
	}
}

void		reset(t_wolf *w)
{
	if (w->reset == 1)
	{
		open_map(w);
		init_c(w);
		key_pos(w);
		w->key = 0;
		w->reset = 0;
	}
}

static void	check_key(t_wolf *w)
{
	if ((int)w->s.sx == 0.0 && (int)w->s.sy == 0.0 && w->key == 0)
	{
		w->key = 1;
		Mix_PlayChannel(4, w->keywav, 0);
	}
}

void		objectives(t_wolf *w)
{
	check_key(w);
	if (w->key == 1)
	{
		SDL_SetRenderDrawColor(w->ren, 255, 255, 255, 255);
		SDL_RenderFillRect(w->ren, &w->t.txt_through);
		if (w->map[(int)(w->c.posx + w->c.dirx * 0.3)][(int)w->c.posy]
				== 3 || w->map[(int)w->c.posx]
				[(int)(w->c.posy + w->c.diry * 0.3)] == 3)
		{
			Mix_PlayChannel(3, w->doorwav, 0);
			if (w->lvln[0] != '4')
			{
				w->reset = 1;
				reset(w);
			}
			else
			{
				SDL_Delay(2309);
				Mix_PlayChannel(3, w->doorwav, 0);
				ft_putendl("Huzzah!");
				w->gameover = 1;
			}
		}
	}
}
