/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 10:23:37 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 20:31:15 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int			put_error(char *s)
{
	ft_putendl_fd(s, 2);
	return (-1);
}

static void	clean_up2(t_wolf *w)
{
	if (w->i.wall != NULL)
		SDL_FreeSurface(w->i.wall);
	if (w->i.door != NULL)
		SDL_FreeSurface(w->i.door);
	if (w->i.wall2 != NULL)
		SDL_FreeSurface(w->i.wall2);
	if (w->s.key != NULL)
		SDL_FreeSurface(w->s.key);
	if (w->level != NULL)
		free(w->level);
	if (w->lvln != NULL)
		free(w->lvln);
	if (w->ren != NULL)
		SDL_DestroyRenderer(w->ren);
	if (w->win != NULL)
		SDL_DestroyWindow(w->win);
	Mix_CloseAudio();
	SDL_Quit();
	ft_putendl("\nThanks to\n   yfuks\n   spariaud\n   mwilk");
	ft_putendl("for beta testing!");
}

int			clean_up(int ret, t_wolf *w)
{
	if (w->mus != NULL)
		Mix_FreeMusic(w->mus);
	if (w->footstep != NULL)
		Mix_FreeChunk(w->footstep);
	if (w->footstep2 != NULL)
		Mix_FreeChunk(w->footstep2);
	if (w->keywav != NULL)
		Mix_FreeChunk(w->keywav);
	if (w->doorwav != NULL)
		Mix_FreeChunk(w->doorwav);
	if (TTF_WasInit() == 1)
	{
		TTF_CloseFont(w->t.font);
		SDL_DestroyTexture(w->t.txt_tex_main);
		SDL_DestroyTexture(w->t.txt_tex_sub);
		TTF_Quit();
	}
	clean_up2(w);
	return (ret);
}

int			get_linenb(int level)
{
	char	*line;
	int		len;

	len = 0;
	line = NULL;
	while (ft_gnl(level, &line) > 0)
	{
		len++;
		free(line);
	}
	free(line);
	close(level);
	return (len);
}

void		init_more(t_wolf *w)
{
	w->lvln = ft_strdup("/");
	w->mus = NULL;
	w->footstep = NULL;
	w->footstep2 = NULL;
	w->doorwav = NULL;
	w->keywav = NULL;
}
