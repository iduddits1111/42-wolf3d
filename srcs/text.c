/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   text.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/29 18:31:20 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 18:06:10 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	text_sub(t_wolf *w)
{
	int	txtw;
	int	txth;

	TTF_SizeText(w->t.font, "Find the key.", &txtw, &txth);
	w->t.txt_rect_sub.x = 10;
	w->t.txt_rect_sub.y = 44;
	w->t.txt_rect_sub.w = txtw;
	w->t.txt_rect_sub.h = txth;
	w->t.txt_through.x = 4;
	w->t.txt_through.y = 60;
	w->t.txt_through.w = txtw + 10;
	w->t.txt_through.h = 3;
	if ((w->t.txt_sub = TTF_RenderText_Solid(w->t.font,
					"Find the key.", w->t.txt_color)) == NULL)
		put_error("Error: text could not be rendered.");
	else
		w->t.txt_tex_sub = SDL_CreateTextureFromSurface(w->ren, w->t.txt_sub);
	SDL_FreeSurface(w->t.txt_sub);
}

static void	text_main(t_wolf *w)
{
	int	txtw;
	int	txth;

	TTF_SizeText(w->t.font, "You must escape the dungeon...", &txtw, &txth);
	w->t.txt_rect_main.x = 10;
	w->t.txt_rect_main.y = 10;
	w->t.txt_rect_main.w = txtw;
	w->t.txt_rect_main.h = txth;
	if ((w->t.txt_main = TTF_RenderText_Solid(w->t.font,
					"You must escape the dungeon...", w->t.txt_color)) == NULL)
		put_error("Error: text could not be rendered.");
	else
		w->t.txt_tex_main = SDL_CreateTextureFromSurface(w->ren, w->t.txt_main);
	SDL_FreeSurface(w->t.txt_main);
}

int			text_init(t_wolf *w)
{
	if (TTF_Init() == -1)
		return (put_error("Error: SDL_ttf failed to initialize."));
	if ((w->t.font = TTF_OpenFont("data/font.ttf", 32)) == NULL)
		return (put_error("Error: font could not be loaded."));
	text_main(w);
	text_sub(w);
	return (0);
}

void		render_text(t_wolf *w)
{
	SDL_RenderCopy(w->ren, w->t.txt_tex_main, NULL, &w->t.txt_rect_main);
	SDL_RenderCopy(w->ren, w->t.txt_tex_sub, NULL, &w->t.txt_rect_sub);
}
