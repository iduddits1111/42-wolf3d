/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/27 16:01:10 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 14:29:26 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void		draw_top(int x, t_wolf *w)
{
	SDL_SetRenderDrawColor(w->ren, 0, 0, 0, 255);
	while (w->r.top > 0 && w->r.y < WIN_Y)
	{
		SDL_RenderDrawPoint(w->ren, x, w->r.y);
		w->r.y++;
		w->r.top--;
	}
}

static void		draw_wall(int x, int line_tmp, t_wolf *w)
{
	Uint32	color;
	int		tx;
	double	ty;
	Uint32	rgb[3];

	while (w->r.line_h > 0 && w->r.y < WIN_Y)
	{
		ty = 1.0 - (double)w->r.line_h / (double)line_tmp;
		tx = (int)(w->r.wallx * TEX_W) + (int)(ty * TEX_W) * TEX_W;
		color = getpixel(tx, ty, w);
		make_color(color, rgb, 0, w);
		SDL_SetRenderDrawColor(w->ren, rgb[0] / w->r.fog, rgb[1] / w->r.fog,
				rgb[2] / w->r.fog, 255);
		SDL_RenderDrawPoint(w->ren, x, w->r.y);
		w->r.line_h--;
		w->r.y++;
	}
}

static void		draw_bot(int x, t_wolf *w)
{
	while (w->r.y < WIN_Y)
	{
		w->r.fog = WIN_Y / (2.0 * w->r.y - WIN_Y + w->m.look * 2) * 1.2;
		if (w->r.fog > 254)
			w->r.fog = 254;
		else if (w->r.fog < 1)
			w->r.fog = 1;
		SDL_SetRenderDrawColor(w->ren, 33 / w->r.fog, 33 / w->r.fog,
				33 / w->r.fog, 255);
		SDL_RenderDrawPoint(w->ren, x, w->r.y);
		w->r.y++;
	}
}

void			draw(int x, t_wolf *w)
{
	int		line_tmp;

	w->s.zbuf[x] = w->r.wall_dist;
	line_tmp = w->r.line_h;
	w->r.fog = w->r.wall_dist / 0.9 * 0.75;
	if (w->r.fog > 254)
		w->r.fog = 254;
	else if (w->r.fog < 1)
		w->r.fog = 1;
	w->r.top = (WIN_X / 2) - (w->r.line_h / 2) - w->m.look;
	w->r.y = 0;
	draw_top(x, w);
	if (w->r.top < 0)
		w->r.line_h += w->r.top;
	draw_wall(x, line_tmp, w);
	draw_bot(x, w);
}
