/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/07 10:03:57 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 17:04:37 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static int	check_map2(int i, t_wolf *w)
{
	i = 0;
	while (i < 24)
	{
		if (w->map[0][i] <= 1)
			return (put_error("Error: wrong map format"));
		if (w->map[i][0] <= 1)
			return (put_error("Error: wrong map format"));
		if (w->map[23][i] <= 1)
			return (put_error("Error: wrong map format"));
		if (w->map[i][23] <= 1)
			return (put_error("Error: wrong map format"));
		i++;
	}
	return (0);
}

static int	check_map(t_wolf *w)
{
	int	i;
	int	j;
	int	c0;
	int	c1;

	i = 0;
	c0 = 0;
	c1 = 0;
	while (i < 24)
	{
		j = 0;
		while (j < 24)
		{
			w->map[i][j] == 0 ? c0++ : c1++;
			j++;
		}
		i++;
	}
	if (c1 == 576 || c0 == 576)
		return (put_error("Error: wrong map format"));
	if (w->map[(int)w->c.posx][(int)w->c.posy] != 0)
		return (put_error("Error: wrong map format"));
	return (check_map2(i, w));
}

static int	get_mapnb(char c)
{
	int	nb;

	nb = 0;
	if (ft_isdigit(c) == 1)
	{
		nb *= 10;
		nb = nb + c - '0';
	}
	else
		return (-1);
	return (nb);
}

static int	stock_map(int fd, t_wolf *w)
{
	int		i;
	int		j;
	char	*line;

	i = 0;
	while (ft_gnl(fd, &line))
	{
		j = 0;
		if (ft_strlen(line) != MAP_W)
			return (put_error("Error: wrong map format."));
		while (line[j])
		{
			if ((w->map[i][j] = get_mapnb(line[j])) == -1)
				return (put_error("Error: wrong map format."));
			j++;
		}
		i++;
		free(line);
	}
	free(line);
	if (check_map(w) == -1)
		return (-1);
	key_pos(w);
	return (0);
}

int			open_map(t_wolf *w)
{
	w->lvln[0]++;
	w->level = ft_strjoin("./maps/level", w->lvln);
	if ((w->fd = open(w->level, O_RDWR)) == -1)
		return (put_error("Error: unable to open level."));
	if (get_linenb(w->fd) != MAP_H)
	{
		close(w->fd);
		return (put_error("Error: wrong map format."));
	}
	if ((w->fd = open(w->level, O_RDWR)) > 0)
	{
		if (stock_map(w->fd, w) == -1)
		{
			close(w->fd);
			return (-1);
		}
		close(w->fd);
		free(w->level);
		w->level = NULL;
		return (0);
	}
	return (put_error("Error: unable to open level."));
}
