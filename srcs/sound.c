/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sound.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mborde <mborde@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/29 13:54:42 by mborde            #+#    #+#             */
/*   Updated: 2015/06/12 20:28:14 by mborde           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	play_sound(t_wolf *w)
{
	if ((w->m.forw || w->m.left || w->m.back || w->m.right) && !w->m.run)
		Mix_Resume(1);
	else if ((!w->m.forw && !w->m.left && !w->m.back && !w->m.right) ||
			w->m.run)
		Mix_Pause(1);
	if (w->m.run && (w->m.forw || w->m.left || w->m.back || w->m.right))
		Mix_Resume(2);
	else if (!w->m.run || (!w->m.forw && !w->m.left && !w->m.back &&
				!w->m.right))
		Mix_Pause(2);
}

int		sound_init(t_wolf *w)
{
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
		return (put_error("Error:  SDL_mixer failed to initialize."));
	if ((w->mus = Mix_LoadMUS("data/music.wav")) == NULL)
		return (put_error("Error: music.wav could not be loaded."));
	if ((w->footstep = Mix_LoadWAV("data/footstep.wav")) == NULL)
		return (put_error("Error: footstep.wav could not be loaded."));
	if ((w->footstep2 = Mix_LoadWAV("data/footstep2.wav")) == NULL)
		return (put_error("Error: footstep2.wav could not be loaded."));
	if ((w->doorwav = Mix_LoadWAV("data/door.wav")) == NULL)
		return (put_error("Error: door.wav could not be loaded."));
	if ((w->keywav = Mix_LoadWAV("data/key.wav")) == NULL)
		return (put_error("Error: key.wav could not be loaded."));
	Mix_PlayMusic(w->mus, -1);
	Mix_PlayChannel(1, w->footstep, -1);
	Mix_Pause(1);
	Mix_PlayChannel(2, w->footstep2, -1);
	Mix_Pause(2);
	return (0);
}
